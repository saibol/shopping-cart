package com.sai.shopping.util;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class CommonUtil {

	public static final int PRECISION = 3;
	private static CommonUtil commonUtil = new CommonUtil();

	private CommonUtil() {}

	public static Double roundUp(Double val) {
		return BigDecimal.valueOf(val).setScale(PRECISION, RoundingMode.HALF_UP).doubleValue();
	}

	public File getFile(String fileName) {
		ClassLoader classLoader = getClass().getClassLoader();
		return new File(classLoader.getResource(fileName).getFile());
	}

	public static CommonUtil getInstance() {
		return commonUtil;
	}
}