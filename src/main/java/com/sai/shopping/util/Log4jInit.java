package com.sai.shopping.util;

import java.util.logging.Logger;

import org.apache.log4j.xml.DOMConfigurator;

import com.sai.shopping.constant.ShoppingConstant;

public class Log4jInit {
	private static final Logger LOGGER = Logger.getLogger(Log4jInit.class.getName());
	private CommonUtil commonUtil = CommonUtil.getInstance();

	public void init() {
		DOMConfigurator.configure(commonUtil
				.getFile(ShoppingConstant.LOG_FILE)
				.getAbsolutePath());
		LOGGER.info("logger initialized");
	}
}
