package com.sai.shopping;

import java.io.File;

import org.apache.log4j.Logger;

import com.sai.shopping.bm.CategoryBM;
import com.sai.shopping.bm.DiscountBasedOnCategory;
import com.sai.shopping.bm.DiscountBasedOnTotalCartValue;
import com.sai.shopping.bm.ItemBM;
import com.sai.shopping.bm.SlabBM;
import com.sai.shopping.constant.ShoppingConstant;
import com.sai.shopping.model.ShoppingCart;
import com.sai.shopping.util.CommonUtil;
import com.sai.shopping.util.Log4jInit;

public class ShoppingApp {

	private ItemBM itemBM;
	private SlabBM slabBM;
	private DiscountBasedOnCategory discOnCat;
	private DiscountBasedOnTotalCartValue discOnTotal;
	private CategoryBM categoryBM;
	private ShoppingCart cart;
	private File itemInputFile;
	private CommonUtil commonUtil = CommonUtil.getInstance();
	
	static Logger logger = Logger.getLogger(ShoppingApp.class);

	public ShoppingApp() {
	}
	
	private void init() {
		new Log4jInit().init();
		this.categoryBM = new CategoryBM();
		this.itemBM = new ItemBM();
		this.slabBM = new SlabBM();
		this.discOnCat = new DiscountBasedOnCategory(categoryBM);
		this.discOnTotal = new DiscountBasedOnTotalCartValue(itemBM, slabBM, discOnCat);
		this.cart = new ShoppingCart();
		itemInputFile = commonUtil.getFile(ShoppingConstant.ITEM_LIST_INPUT_FILE);
	}

	private void process() {
		init();
		cart.setItems(itemBM.getItems(itemInputFile));

		cart.getItems().stream().forEach(x -> {
			logger.info(x);
			logger.info("category discount=" + discOnCat.getDiscPerc(x) + "%, disc Amt="+discOnCat.getDiscValue(x)+", itemNetAmt="+discOnCat.getNetValue(x));
		});

		Double grossTotal = cart.getItems().stream().mapToDouble(x -> discOnCat.getGrossValue(x)).sum();
		logger.info("\nGross Total (before category discount) : " + grossTotal);

		Double netTotal = cart.getItems().stream().mapToDouble(x -> discOnCat.getNetValue(x)).sum();
		logger.info("Net Total (after category discount) : " + CommonUtil.roundUp(netTotal));

		logger.info("Flat discount=" + discOnTotal.getDiscPerc(netTotal) + "%, flatDiscountAmt= "+discOnTotal.getDiscValue());
		logger.info("Final Total Amount Payable : " + CommonUtil.roundUp(discOnTotal.getNetValue()));
	}

	public static void main(String[] args) {
		new ShoppingApp().process();
	}
}





