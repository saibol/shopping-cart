package com.sai.shopping.bm;

import java.util.Objects;

import com.sai.shopping.model.Category;
import com.sai.shopping.model.Item;

public class DiscountBasedOnCategory implements BaseDiscount<Item, Float> {

	private CategoryBM categoryBM;

	public DiscountBasedOnCategory(CategoryBM categoryBM) {
		this.categoryBM = categoryBM;
	}

	@Override
	public Float getDiscPerc(Item item) {
		Category category = categoryBM.getCategory(item);
		return Objects.nonNull(category) ? category.getDiscPerc() : 0;
	}

	public double getGrossValue(Item item) {
		return item.getQuantity() * item.getUnitPrice();
	}

	public double getNetValue(Item item) {
		return getGrossValue(item) - getDiscValue(item);
	}

	public double getDiscValue(Item item) {
		return getGrossValue(item) * getDiscPerc(item) / 100;
	}
}
