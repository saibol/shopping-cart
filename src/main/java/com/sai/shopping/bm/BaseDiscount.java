package com.sai.shopping.bm;

public interface BaseDiscount<T, U> {
	U getDiscPerc(T t);
}
