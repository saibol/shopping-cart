package com.sai.shopping.bm;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sai.shopping.constant.ShoppingConstant;
import com.sai.shopping.model.FlatDiscountSlabs;
import com.sai.shopping.model.Slab;
import com.sai.shopping.util.CommonUtil;

public class SlabBM {
	private static Logger logger = Logger.getLogger(SlabBM.class);
	
	public List<Slab> getAllSlabs(File slabFile) {
		FlatDiscountSlabs slabs = new FlatDiscountSlabs();
		try {
			slabs = new XmlMapper().readValue(
					StringUtils.toEncodedString(
							Files.readAllBytes(Paths.get(slabFile.getAbsolutePath())),
							StandardCharsets.UTF_8),
					FlatDiscountSlabs.class);
		} catch (Exception e) {
			logger.error("Error occurred while getting discount file");
			e.printStackTrace();
		}
		return slabs.getSlab();
	}
	
	public Slab getSlab(Double totalValue) {
		File slabFile = CommonUtil.getInstance().getFile(ShoppingConstant.FLAT_DISC_SLAB_INPUT_FILE);
		List<Slab> resultSlab = getAllSlabs(slabFile)
				.stream()
				.filter(x -> isValueInRange(x, totalValue))
				.collect(Collectors.toList());
		return (CollectionUtils.isEmpty(resultSlab)) 
				? null 
				: resultSlab.get(0);
	}
	
	private boolean isValueInRange(Slab slab, Double totalValue) {
		if (Objects.nonNull(slab.getRangeMin()) && Objects.nonNull(slab.getRangeMax()))
			return slab.getRangeMin() <= totalValue && totalValue <= slab.getRangeMax();
		else
			return (Objects.isNull(slab.getRangeMax())) ? slab.getRangeMin() <= totalValue : false;
	}
}
