package com.sai.shopping.bm;

import java.io.File;
import java.util.Objects;

import com.sai.shopping.constant.ShoppingConstant;
import com.sai.shopping.model.Slab;
import com.sai.shopping.util.CommonUtil;

public class DiscountBasedOnTotalCartValue implements BaseDiscount<Double, Float> {

	private ItemBM itemBM;
	private SlabBM slabBM;
	private DiscountBasedOnCategory discOnCat;

	public DiscountBasedOnTotalCartValue(ItemBM itemBM, SlabBM slabBM, DiscountBasedOnCategory discOnCat) {
		this.itemBM = itemBM;
		this.slabBM = slabBM;
		this.discOnCat = discOnCat;
	}

	@Override
	public Float getDiscPerc(Double totalValue) {
		Slab slab = slabBM.getSlab(totalValue);
		return Objects.nonNull(slab) ? slab.getDiscPerc() : 0;
	}

	public double getGrossValue() {
		File itemInputFile = CommonUtil.getInstance()
				.getFile(ShoppingConstant.ITEM_LIST_INPUT_FILE);
		return itemBM.getItems(itemInputFile)
				.stream()
				.mapToDouble(x -> discOnCat.getNetValue(x))
				.sum();
	}

	public double getNetValue() {
		return getGrossValue() - getDiscValue();
	}

	public double getDiscValue() {
		return getGrossValue() * getDiscPerc(getGrossValue()) / 100;
	}
}