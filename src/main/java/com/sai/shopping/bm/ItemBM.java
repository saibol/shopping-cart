package com.sai.shopping.bm;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sai.shopping.model.Item;
import com.sai.shopping.model.ShoppingCart;

public class ItemBM {
	
	private static Logger logger = Logger.getLogger(ItemBM.class);
	
	public List<Item> getItems(File itemListFile){
		ShoppingCart cart = new ShoppingCart();
		try {
			cart = new XmlMapper()
					.readValue(StringUtils.toEncodedString(Files.readAllBytes(Paths.get(itemListFile.getAbsolutePath())),
							StandardCharsets.UTF_8), ShoppingCart.class);
		} catch (Exception e) {
			logger.error("Error occurred while getting Item file");
			e.printStackTrace();
		}
		return cart.getItems();
	}
}
