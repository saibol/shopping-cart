package com.sai.shopping.bm;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sai.shopping.constant.ShoppingConstant;
import com.sai.shopping.model.Categories;
import com.sai.shopping.model.Category;
import com.sai.shopping.model.Item;
import com.sai.shopping.util.CommonUtil;

public class CategoryBM {

	private CommonUtil commonUtil = CommonUtil.getInstance();
	private static Logger logger = Logger.getLogger(CategoryBM.class);

	public List<Category> getAllCategories() {
		Categories categories = new Categories();
		try {
			File categoryFile = commonUtil.getFile(ShoppingConstant.CATEGORY_INPUT_FILE);
			categories = new XmlMapper().readValue(
					StringUtils.toEncodedString(Files.readAllBytes(Paths.get(categoryFile.getAbsolutePath())),
							StandardCharsets.UTF_8),
					Categories.class);
		} catch (Exception e) {
			logger.error("Error occurred while getting Category file");
			e.printStackTrace();
		}
		return categories.getCategory();
	}

	public Category getCategory(Item item) {
		List<Category> itemCategory = getAllCategories()
				.stream()
				.filter(x -> x.getCategoryId().equals(item.getItemCategoryId()))
				.collect(Collectors.toList());
		return (CollectionUtils.isEmpty(itemCategory)) 
				? null 
				: itemCategory.get(0);
	}
}
