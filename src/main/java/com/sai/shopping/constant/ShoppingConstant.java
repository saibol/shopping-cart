package com.sai.shopping.constant;

public class ShoppingConstant {
	public static final String LOG_FILE = "log4j.xml";
	public static final String CATEGORY_INPUT_FILE = "categories-input-file.xml";
	public static final String ITEM_LIST_INPUT_FILE = "shopping-cart-item-list.xml";
	public static final String FLAT_DISC_SLAB_INPUT_FILE = "flat-discount-slabs-input-file.xml";
}
