package com.sai.shopping.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "Categories")
public final class Categories {
	@JacksonXmlElementWrapper(localName = "category", useWrapping = false)
	private List<Category> category = new ArrayList<>();

	public List<Category> getCategory() {
		return category;
	}

	public void setCategory(List<Category> category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Categories [category=" + category + "]";
	}
}
