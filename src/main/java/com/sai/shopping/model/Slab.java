package com.sai.shopping.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Slab {
	@JacksonXmlProperty(localName = "RangeMin")
	private Integer rangeMin;
	@JacksonXmlProperty(localName = "RangeMax")
	private Integer rangeMax;
	@JacksonXmlProperty(localName = "discPerc")
	private Float discPerc;

	public Slab() {
	}

	public Slab(Integer rangeMin, Integer rangeMax, Float discPerc) {
		super();
		this.rangeMin = rangeMin;
		this.rangeMax = rangeMax;
		this.discPerc = discPerc;
	}

	public Integer getRangeMin() {
		return rangeMin;
	}

	public void setRangeMin(Integer rangeMin) {
		this.rangeMin = rangeMin;
	}

	public Integer getRangeMax() {
		return rangeMax;
	}

	public void setRangeMax(Integer rangeMax) {
		this.rangeMax = rangeMax;
	}

	public Float getDiscPerc() {
		return discPerc;
	}

	public void setDiscPerc(Float discPerc) {
		this.discPerc = discPerc;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("Slab [rangeMin=").append(rangeMin).append(", rangeMax=").append(rangeMax)
				.append(", discPerc=").append(discPerc).append("]").toString();
	}
}
