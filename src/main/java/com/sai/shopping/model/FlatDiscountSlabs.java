package com.sai.shopping.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "FlatDiscountSlabs")
public class FlatDiscountSlabs {
	@JacksonXmlElementWrapper(localName = "slab", useWrapping = false)
	private List<Slab> slab = new ArrayList<>();

	public List<Slab> getSlab() {
		return slab;
	}

	public void setSlab(List<Slab> slab) {
		this.slab = slab;
	}

	public FlatDiscountSlabs() {
	}

	@Override
	public String toString() {
		return "FlatDiscountSlabs [slab=" + slab + "]";
	}
}
