package com.sai.shopping.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Category {
	@JacksonXmlProperty(localName = "id")
	private String categoryId;
	@JacksonXmlProperty(localName = "name")
	private String name;
	@JacksonXmlProperty(localName = "discPerc")
	private Float discPerc;
	
	public Category() {}
	
	public Category(String categoryId, String name, Float discPerc) {
		super();
		this.categoryId = categoryId;
		this.name = name;
		this.discPerc = discPerc;
	}
	
	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getDiscPerc() {
		return discPerc;
	}

	public void setDiscPerc(Float discPerc) {
		this.discPerc = discPerc;
	}

	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("Category [id=").append(categoryId)
				.append(", name=").append(name)
				.append(", discPerc=").append(discPerc)
				.append("]")
				.toString();
	}
}
