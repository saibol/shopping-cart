package com.sai.shopping.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Item{
	@JacksonXmlProperty(localName = "itemId")
	private String itemId;
	@JacksonXmlProperty(localName = "itemCategoryId")
	private String itemCategoryId;
	@JacksonXmlProperty(localName = "itemName")
	private String itemName;
	@JacksonXmlProperty(localName = "unitPrice")
	private Float unitPrice;
	@JacksonXmlProperty(localName = "quantity")
	private Integer quantity;
	
	public Item() {}
	public Item(String itemId, String itemCategoryId, String itemName, Float unitPrice, Integer quantity) {
		super();
		this.itemId = itemId;
		this.itemCategoryId = itemCategoryId;
		this.itemName = itemName;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemCategoryId() {
		return itemCategoryId;
	}

	public void setItemCategoryId(String itemCategoryId) {
		this.itemCategoryId = itemCategoryId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Float getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("Item [itemId=").append(itemId)
				.append(", itemCategoryId=").append(itemCategoryId)
				.append(", itemName=").append(itemName)
				.append(", unitPrice=").append(unitPrice)
				.append(", quantity=").append(quantity)
				.append("]")
				.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		Item item = (Item) obj;
		return this.itemCategoryId.equals(item.itemCategoryId) &&
				this.itemId.equals(item.itemId) &&
				this.itemName.equals(item.itemName) &&
				this.quantity.equals(item.quantity) && 
				this.unitPrice.equals(item.unitPrice);
	}
}