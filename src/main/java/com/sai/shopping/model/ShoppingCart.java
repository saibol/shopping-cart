package com.sai.shopping.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "ShoppingCart")
public class ShoppingCart {
	@JacksonXmlElementWrapper(localName = "item", useWrapping = false)
	private List<Item> item = new ArrayList<>();

	@Override
	public String toString() {
		return "ShoppingCart [item=" + item + "]";
	}

	public List<Item> getItem() {
		return item;
	}
	
	public List<Item> getItems() {
		return item;
	}

	public void setItems(List<Item> item) {
		this.item = item;
	}
}