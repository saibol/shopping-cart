package com.sai.shopping.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CommonUtilTest {

	@Test
	public void roundUp_normalNumber() {
		assertEquals(new Double(123), CommonUtil.roundUp(123d));
	}

	@Test
	public void roundUp_fractionalNumberWith4Digit() {
		assertEquals(new Double(123.451), CommonUtil.roundUp(123.4511d));
	}

	@Test
	public void roundUp_fractionalNumberWith2Digit() {
		assertEquals(new Double(123.25), CommonUtil.roundUp(123.25d));
	}

}
