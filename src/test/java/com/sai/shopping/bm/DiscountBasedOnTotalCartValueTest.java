package com.sai.shopping.bm;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import com.sai.shopping.constant.ShoppingConstant;
import com.sai.shopping.model.ShoppingCart;
import com.sai.shopping.util.CommonUtil;

public class DiscountBasedOnTotalCartValueTest {

	private ItemBM itemBM;
	private SlabBM slabBM;
	private DiscountBasedOnCategory discOnCat;
	private DiscountBasedOnTotalCartValue discOnTotal;
	private CategoryBM categoryBM;
	private ShoppingCart cart;
	private File itemFile;

	@Before
	public void init() {
		this.categoryBM = new CategoryBM();
		this.itemBM = new ItemBM();
		this.slabBM = new SlabBM();
		this.discOnCat = new DiscountBasedOnCategory(categoryBM);
		this.discOnTotal = new DiscountBasedOnTotalCartValue(itemBM, slabBM, discOnCat);
		this.cart = new ShoppingCart();
		this.itemFile = CommonUtil.getInstance().getFile(ShoppingConstant.ITEM_LIST_INPUT_FILE);
	}

	@Test
	public void getDiscPerc_whenTotalLessthen7000() {
		cart.setItems(itemBM.getItems(itemFile));
		Double netTotal = cart.getItems().stream().mapToDouble(x -> discOnCat.getNetValue(x)).sum();
		assertEquals(new Float(5.0), discOnTotal.getDiscPerc(netTotal));
	}
}
