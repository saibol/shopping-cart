package com.sai.shopping.bm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import com.sai.shopping.constant.ShoppingConstant;
import com.sai.shopping.model.Item;
import com.sai.shopping.model.ShoppingCart;
import com.sai.shopping.util.CommonUtil;

public class ItemBMTest {

	private ItemBM itemBM;
	private ShoppingCart cart;
	private CommonUtil commonUtil = CommonUtil.getInstance();
	private File itemInputFile;
	
	@Before
	public void init() {
		this.itemBM = new ItemBM();
		this.cart = new ShoppingCart();
		this.itemInputFile = commonUtil.getFile(ShoppingConstant.ITEM_LIST_INPUT_FILE);
	}

	@Test
	public void getItems_size() {
		cart.setItems(itemBM.getItems(itemInputFile));
		assertEquals(5, cart.getItems().size());
	}

	@Test
	public void getItems_singleItem() {
		cart.setItems(itemBM.getItems(itemInputFile));
		Item expectedItem = new Item("000001", "003", "Muesli", 100.0f, 2);
		assertTrue(isItemEqual(expectedItem, cart.getItems().get(0)));
	}

	private boolean isItemEqual(Item item1, Item item2) {
		return item1.equals(item2);
	}

}
