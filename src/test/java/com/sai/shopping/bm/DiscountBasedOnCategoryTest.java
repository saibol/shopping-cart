package com.sai.shopping.bm;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import com.sai.shopping.constant.ShoppingConstant;
import com.sai.shopping.model.Item;
import com.sai.shopping.model.ShoppingCart;
import com.sai.shopping.util.CommonUtil;

public class DiscountBasedOnCategoryTest {

	private ItemBM itemBM;
	private DiscountBasedOnCategory discOnCat;
	private CategoryBM categoryBM;
	private ShoppingCart cart;
	private File itemFile;
	
	@Before
	public void init() {
		this.categoryBM = new CategoryBM();
		this.itemBM = new ItemBM();
		this.discOnCat = new DiscountBasedOnCategory(categoryBM);
		this.cart = new ShoppingCart();
		this.itemFile = CommonUtil.getInstance().getFile(ShoppingConstant.ITEM_LIST_INPUT_FILE);
	}

	@Test
	public void getDiscPerc_whenItemFound() {
		cart.setItems(itemBM.getItems(itemFile));
		Item item = new Item("000001", "003", "Muesli", 100.0f, 2);
		assertEquals(new Float(2.0), discOnCat.getDiscPerc(item));
	}

	@Test
	public void getDiscPerc_whenItemNotFound() {
		cart.setItems(itemBM.getItems(itemFile));
		Item item = new Item("99999", "99", "Muesli", 100.0f, 2);
		assertEquals(new Float(0.0), discOnCat.getDiscPerc(item));
	}
}
