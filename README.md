**Problem Description **

Please write a program that can dynamically apply discounts on shopping cart items, based on their category. Besides, based on the total cart value, the program should be able to apply a final slab-wise discount. 

For simplicity, and to work around database portability, assume the shopping cart data to be available in XML/ JSON format. 

---

For every item added in the shopping cart, apply the discount based on the category of the item being added. Likewise, based on all added items, (& their associated discounts applied), calculate the grand total, based on 3 parameters: (Unit Price, Quantity, & its applicable discount) – for each item. Apply a final slab based discount on the grand total, to generate the net bill value

---

**Inputs, to the program: **

1. XML/ JSON – List of Shopping Item categories, and their applicable discount % 
2. XML/ JSON – List of items in shopping cart, their associated information, including their shopping category 
3. XML/ JSON – Grand total slabs, & their applicable discount % 

---

**Outputs from the program: **

1. Itemized bill – listing each purchase item, its quantity, unit price, discount, and net purchase amount for that item, (& quantity) 
2. Grand total, applicable discount, and Net Bill Amount. 

---

The program should be written in Java. Please provide the source code and unit tests in a runnable state. Feel free to use the accompanying Maven based project, but its use is not required if you are not familiar with Maven. Your unit tests should be written in either JUnit or TestNG.  

You can spend as much time as you like on the project, but typically, we expect you to spend about 2-3 hours to come up with a solution and a backlog of ideas on how to improve the system. 

---

** Sample XMLs to define shopping categories, & discount slabs **

<?xml version="1.0" encoding="UTF-8"?>
<Categories>
   <category>
      <id>001</id>
      <name>ConsumerGoods</name>
      <discPerc>5</discPerc>
   </category>
   <category>
      <id>002</id>
      <name>OrganicFood</name>
      <discPerc>7</discPerc>
   </category>
   <category>
      <id>003</id>
      <name>Grocery</name>
      <discPerc>2</discPerc>
   </category>
   <category>
      <id>004</id>
      <name>BabyProducts</name>
      <discPerc>10</discPerc>
   </category>
   <category>
      <id>005</id>
      <name>Apparrel</name>
      <discPerc>15</discPerc>
   </category>
</Categories>

<?xml version="1.0" encoding="UTF-8"?>
<FlatDiscountslabs>
	<slab>
		<RangeMin>0</RangeMin>
		<RangeMax>3000</RangeMax>
		<discPerc>2</discPerc>
	</slab>
	<slab>
		<RangeMin>3001</RangeMin>
		<RangeMax>7000</RangeMax>
		<discPerc>4</discPerc>
	</slab>
	<slab>
		<RangeMin>7000</RangeMin>
		<RangeMax></RangeMax>
		<discPerc>5</discPerc>
	</slab>
</FlatDiscountslabs> 

---
**Sample Shopping Cart Items List **

<?xml version="1.0" encoding="UTF-8"?>
<ShoppingCart>
	<item>	
		<itemId>000001</itemId>
		<itemCategoryId>003</itemCategoryId>
		<itemName>Muesli</itemName>
		<unitPrice>100</unitPrice>
		<quantity>2</quantity>
	</item>
	<item>
		<itemId>000002</itemId>
		<itemCategoryId>005</itemCategoryId>
		<itemName>Mens Tshirt Arrow 3463</itemName>
		<unitPrice>1536</unitPrice>
		<quantity>1</quantity>
	</item>
	<item>
		<itemId>000003</itemId>
		<itemCategoryId>002</itemCategoryId>
		<itemName>Organic Tomatoes</itemName>
		<unitPrice>10</unitPrice>
		<quantity>2</quantity>
	</item>
	<item>
		<itemId>000004</itemId>
		<itemCategoryId>001</itemCategoryId>
		<itemName>Wipro CFL</itemName>
		<unitPrice>120</unitPrice>
		<quantity>1</quantity>
	</item>
	<item>
		<itemId>000005</itemId>
		<itemCategoryId>001</itemCategoryId>
		<itemName>Wipro CFL2</itemName>
		<unitPrice>12000</unitPrice>
		<quantity>1</quantity>
	</item>
</ShoppingCart>
---
